package com.lookalikeme.app.Users;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.lookalikeme.app.Main_Menu.RelateToFragment_OnBack.RootFragment;
import com.lookalikeme.app.R;
import com.labo.kaji.fragmentanimations.MoveAnimation;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */

// in this class we will get all the details of spacific user and show it here
public class User_detail_F extends RootFragment implements View.OnClickListener {


    View view;
    Context context;

    ImageButton  move_downbtn;

    RelativeLayout  username_layout;
    ScrollView scrollView;

    TextView username_txt, bottom_age,bottom_gender_txt,bottom_location_text,bottom_about_txt;

    SliderLayout sliderLayout;

    PagerIndicator pagerIndicator;

    Nearby_User_Get_Set data_item;

    LinearLayout birthday_layout,gender_layout;

    public User_detail_F() {

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_user_detail_, container, false);
        context = getContext();

        Bundle bundle=getArguments();
        if(bundle!=null)
        {
            // get all the data of the user from privious fragment and show it here
         data_item= (Nearby_User_Get_Set) bundle.getSerializable("data");
        }

        scrollView = view.findViewById(R.id.scrollView);
        username_layout = view.findViewById(R.id.username_layout);

        move_downbtn = view.findViewById(R.id.move_downbtn);
        move_downbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 getActivity().onBackPressed();


            }
        });



       sliderLayout = view.findViewById(R.id.slider);
       pagerIndicator=view.findViewById(R.id.custom_indicator);

        sliderLayout.setVisibility(View.INVISIBLE);


        YoYo.with(Techniques.BounceInDown)
                .duration(800)
                .playOn(move_downbtn);

        init_bottom_view();


        return view;

    }


    // this method will initialize all the views and set the data in that view
    public void init_bottom_view() {
        username_txt = view.findViewById(R.id.username_txt);
        username_txt.setText(data_item.getName());


        birthday_layout=view.findViewById(R.id.birthday_layout);
        bottom_age = view.findViewById(R.id.bottom_age);

        if(data_item.getBirthday().equals(""))
         birthday_layout.setVisibility(View.GONE);
        else
        bottom_age.setText(""+data_item.getBirthday());



        gender_layout=view.findViewById(R.id.gender_layout);
        bottom_gender_txt = view.findViewById(R.id.bottom_gender_txt);

        if(data_item.getGender().equals(""))
        gender_layout.setVisibility(View.GONE);
        else
            bottom_gender_txt.setText(""+data_item.getGender());


        bottom_location_text=view.findViewById(R.id.bottom_location_txt);
        bottom_location_text.setText(data_item.getLocation());

        bottom_about_txt=view.findViewById(R.id.bottom_about_txt);
        bottom_about_txt.setText(data_item.getAbout());

    }


    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (enter) {
            Animation anim= MoveAnimation.create(MoveAnimation.UP, enter, 200);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {}
                @Override
                public void onAnimationEnd(Animation animation) {
                    // when animation is done then we will show the picture slide
                    // becuase animation in that case will show fulently

                    fill_data();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {}
            });
            return anim;

        } else {
            return MoveAnimation.create(MoveAnimation.DOWN, enter, 200);
        }
    }



    // this method will set and show the pictures slider
    public void fill_data(){
        sliderLayout.setCustomIndicator(pagerIndicator);
        sliderLayout.stopAutoCycle();
        ArrayList<String> file_maps=new ArrayList<>() ;
        file_maps=data_item.getImagesurl();
        for (int i = 0; i < file_maps.size(); i++) {
            DefaultSliderView defaultSliderView = new DefaultSliderView(context);
            defaultSliderView
                    .image(file_maps.get(i))
                    .setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            defaultSliderView.bundle(new Bundle());
            sliderLayout.addSlider(defaultSliderView);
        }
        sliderLayout.setVisibility(View.VISIBLE);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
        }
    }


}