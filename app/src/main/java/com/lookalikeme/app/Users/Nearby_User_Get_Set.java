package com.lookalikeme.app.Users;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by AQEEL on 10/24/2018.
 */


public class Nearby_User_Get_Set implements Serializable {

    String fb_id,first_name,last_name,name,birthday,gender,about,location;
    ArrayList<String> imagesurl;

    public String getFb_id() {
        return fb_id;
    }

    public void setFb_id(String fb_id) {
        this.fb_id = fb_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public ArrayList<String> getImagesurl() {
        return imagesurl;
    }

    public void setImagesurl(ArrayList<String> imagesurl) {
        this.imagesurl = imagesurl;
    }
}

