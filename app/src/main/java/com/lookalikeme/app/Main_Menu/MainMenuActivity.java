package com.lookalikeme.app.Main_Menu;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.lookalikeme.app.Functions;
import com.lookalikeme.app.R;
import com.lookalikeme.app.Variables;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class MainMenuActivity extends AppCompatActivity implements BillingProcessor.IBillingHandler {
    private MainMenuFragment mainMenuFragment;
    long mBackPressed;


    public static SharedPreferences sharedPreferences;
    public static String user_id;
    public static String user_name;
    public static String birthday;
    public static String token;


    DatabaseReference rootref;

    BillingProcessor billingProcessor;

    public static boolean purduct_purchase=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        // here we will make the static variable of user info which will user in different
        // places during using app
        sharedPreferences = getSharedPreferences(Variables.pref_name, MODE_PRIVATE);
        user_id = sharedPreferences.getString(Variables.uid, "null");
        user_name = sharedPreferences.getString(Variables.f_name, "") + " " +
                sharedPreferences.getString(Variables.l_name, "");
        birthday=sharedPreferences.getString(Variables.birth_day,"");
        token=sharedPreferences.getString(Variables.device_token,"null");
        rootref= FirebaseDatabase.getInstance().getReference();

        // we will get the user billing status in local if it not present inn local then we will intialize the billing
        if(sharedPreferences.getBoolean(Variables.ispuduct_puchase,false)){
            purduct_purchase=true;
        }else {
            // check user if subscript or not both status we will save
            billingProcessor = new BillingProcessor(this, Variables.licencekey, this);
            billingProcessor.initialize();
        }

        if (savedInstanceState == null) {

            initScreen();

        } else {
            mainMenuFragment = (MainMenuFragment) getSupportFragmentManager().getFragments().get(0);
        }

         }


    private void initScreen() {
        mainMenuFragment = new MainMenuFragment();
        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, mainMenuFragment)
                .commit();

        findViewById(R.id.container).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.hideSoftKeyboard(MainMenuActivity.this);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        rootref.child("Users").child(user_id).child("token").setValue(token);
    }


    @Override
    public void onBackPressed() {
        if (!mainMenuFragment.onBackPressed()) {
            int count = this.getSupportFragmentManager().getBackStackEntryCount();
            if (count == 0) {
                if (mBackPressed + 2000 > System.currentTimeMillis()) {
                    super.onBackPressed();
                    return;
                } else {
//                    Toast.makeText(getBaseContext(), "Tap Again To Exit", Toast.LENGTH_SHORT).show();
                    mBackPressed = System.currentTimeMillis();

                }
            } else {
                super.onBackPressed();
            }
        }

    }






    // below all method is belong to get the info that user is subscribe our app or not
    // keep in mind it is a listener so we will close the listener after checking in onBillingInitialized method
    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
        sharedPreferences.edit().putBoolean(Variables.ispuduct_puchase,true).commit();
        purduct_purchase=true;
    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {

    }

    @Override
    public void onBillingInitialized() {
        if(billingProcessor.loadOwnedPurchasesFromGoogle()){
            if(billingProcessor.isSubscribed(Variables.product_ID)){
                sharedPreferences.edit().putBoolean(Variables.ispuduct_puchase,true).commit();
                purduct_purchase=true;
                billingProcessor.release();
            }
    }
}

    @Override
    protected void onDestroy() {

        if (billingProcessor != null) {
            billingProcessor.release();
        }
        super.onDestroy();
    }


}
