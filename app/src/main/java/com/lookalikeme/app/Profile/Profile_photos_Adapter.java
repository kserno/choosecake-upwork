package com.lookalikeme.app.Profile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.lookalikeme.app.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by AQEEL on 7/16/2018.
 */

public class Profile_photos_Adapter extends RecyclerView.Adapter<Profile_photos_Adapter.HistoryviewHolder> {
    Context context;

    ArrayList<String> photos;

    private Profile_photos_Adapter.OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(String item,int postion,View view);
    }


    public Profile_photos_Adapter(Context context,ArrayList<String> arrayList,Profile_photos_Adapter.OnItemClickListener listener)  {
        this.context=context;
        photos=arrayList;
        this.listener=listener;
    }

    @Override
    public Profile_photos_Adapter.HistoryviewHolder onCreateViewHolder(ViewGroup viewGroup, int viewtype) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_edit_profile_layout,null);
        Profile_photos_Adapter.HistoryviewHolder viewHolder = new Profile_photos_Adapter.HistoryviewHolder(view);
        return viewHolder;

    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    @Override
    public void onBindViewHolder(final Profile_photos_Adapter.HistoryviewHolder holder, final int position) {
        holder.bind(photos.get(position),position,listener);
        if(position==0){
            holder.crossbtn.setVisibility(View.GONE);
        }else {
            holder.crossbtn.setVisibility(View.VISIBLE);
        }
        if(photos.get(position).equals("")){
            holder.crossbtn.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_round_add_btn));
            Picasso.with(context).load("null").placeholder(R.drawable.image_placeholder).centerCrop().resize(200,300).into(holder.image);

        }else {
            holder.crossbtn.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_cross));
            Picasso.with(context).load(photos.get(position)).placeholder(R.drawable.image_placeholder).centerCrop().resize(200,300).into(holder.image);
        }
     }
    /**
     * Inner Class for a recycler view
     */
    class HistoryviewHolder extends RecyclerView.ViewHolder {
        View view;
        ImageView image;
        ImageButton crossbtn;
        public HistoryviewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image=view.findViewById(R.id.image);
            crossbtn=view.findViewById(R.id.cross_btn);
        }


        public void bind(final String item, final int position , final Profile_photos_Adapter.OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item,position,v);
                }
            });

            crossbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item,position,v);
                }
            });
        }


    }

}

