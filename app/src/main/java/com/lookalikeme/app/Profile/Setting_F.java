package com.lookalikeme.app.Profile;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.lookalikeme.app.Accounts.Login_A;
import com.lookalikeme.app.Main_Menu.MainMenuActivity;
import com.lookalikeme.app.R;
import com.lookalikeme.app.Variables;
import com.zhouyou.view.seekbar.SignSeekBar;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */

public class Setting_F extends Fragment {


    SwitchCompat men_switch,women_switch,show_me_on_binder;


    SignSeekBar distance_bar,age_seekbar;
    TextView age_range_txt,distance_txt;


    View view;
    Context context;

    ImageButton back_btn;


    LinearLayout logout_btn;

    public Setting_F() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_setting, container, false);
        context=getContext();


        back_btn=view.findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        men_switch=view.findViewById(R.id.men_switch);
        women_switch=view.findViewById(R.id.women_switch);

        if(MainMenuActivity.sharedPreferences.getString(Variables.show_me,"all").equals("all")){
            men_switch.setChecked(true);
            women_switch.setChecked(true);
        }
        else if(MainMenuActivity.sharedPreferences.getString(Variables.show_me,"all").equals("male")){
            men_switch.setChecked(true);
        }
        else {
            women_switch.setChecked(true);
        }


        men_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(!isChecked && !women_switch.isChecked()){
                    women_switch.setChecked(true);
                }else {
                    set_Show_me_data();
                }
            }
        });


        women_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(!isChecked && !men_switch.isChecked()){
                    men_switch.setChecked(true);
                }else {
                    set_Show_me_data();
                }
            }
        });






        distance_bar=view.findViewById(R.id.distance_bar);
        distance_txt=view.findViewById(R.id.distance_txt);
        distance_bar.setProgress(MainMenuActivity.sharedPreferences.getInt(Variables.max_distance,100));
        distance_txt.setText(MainMenuActivity.sharedPreferences.getInt(Variables.max_distance,100)+" miles");

        distance_bar.setOnProgressChangedListener(new SignSeekBar.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(SignSeekBar signSeekBar, int progress, float progressFloat,boolean fromUser) {
                distance_txt.setText(progress+" miles");
            }

            @Override
            public void getProgressOnActionUp(SignSeekBar signSeekBar, int progress, float progressFloat) {
                MainMenuActivity.sharedPreferences.edit().putInt(Variables.max_distance,progress).commit();
            }

            @Override
            public void getProgressOnFinally(SignSeekBar signSeekBar, int progress, float progressFloat,boolean fromUser) {

                }
        });




        age_seekbar=view.findViewById(R.id.age_seekbar);
        age_range_txt=view.findViewById(R.id.age_range_txt);
        age_seekbar.setProgress(MainMenuActivity.sharedPreferences.getInt(Variables.max_age,60));
        age_range_txt.setText(MainMenuActivity.sharedPreferences.getInt(Variables.max_age,60)+" Years");
        age_seekbar.setOnProgressChangedListener(new SignSeekBar.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(SignSeekBar signSeekBar, int progress, float progressFloat,boolean fromUser) {
                age_range_txt.setText(progress+" Years");
            }

            @Override
            public void getProgressOnActionUp(SignSeekBar signSeekBar, int progress, float progressFloat) {
                MainMenuActivity.sharedPreferences.edit().putInt(Variables.max_age,progress).commit();
            }

            @Override
            public void getProgressOnFinally(SignSeekBar signSeekBar, int progress, float progressFloat,boolean fromUser) {

            }
        });







        show_me_on_binder=view.findViewById(R.id.show_me_on_binder);
        show_me_on_binder.setChecked(MainMenuActivity.sharedPreferences.getBoolean(Variables.show_me_on_binder,true));
        show_me_on_binder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                MainMenuActivity.sharedPreferences.edit().putBoolean(Variables.show_me_on_binder,isChecked).commit();
                if(isChecked){
                    Call_Api_For_show_on_binder_or_not("0");
                }else {
                    Call_Api_For_show_on_binder_or_not("1");
                }
            }
        });




        logout_btn=view.findViewById(R.id.logout_btn);
        logout_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // on press logout btn we will chat the local value and move the user to login screen
                MainMenuActivity.sharedPreferences.edit().putBoolean(Variables.islogin,false).commit();
                startActivity(new Intent(getActivity(), Login_A.class));
                getActivity().overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
                getActivity().finish();
            }
        });


        return view;

    }



    public void set_Show_me_data(){
        if(men_switch.isChecked() && women_switch.isChecked()){
            MainMenuActivity.sharedPreferences.edit().putString(Variables.show_me,"all").commit();

        }else if(!men_switch.isChecked() && women_switch.isChecked()){
            MainMenuActivity.sharedPreferences.edit().putString(Variables.show_me,"female").commit();
        }else if(men_switch.isChecked() && !women_switch.isChecked()){
            MainMenuActivity.sharedPreferences.edit().putString(Variables.show_me,"male").commit();
        }
    }



    private void Call_Api_For_show_on_binder_or_not(String status) {
        JSONObject parameters = new JSONObject();
        try {
            parameters.put("fb_id", MainMenuActivity.user_id);
            parameters.put("status",status);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestQueue rq = Volley.newRequestQueue(context);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, Variables.show_or_hide_profile, parameters, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        String respo=response.toString();

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Log.d("respo",error.toString());
                    }
                });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(jsonObjectRequest);
    }




}
