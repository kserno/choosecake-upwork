package com.lookalikeme.app.Firebase_Notification;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.lookalikeme.app.Chat.Chat_Activity;
import com.lookalikeme.app.Main_Menu.MainMenuActivity;
import com.lookalikeme.app.R;
import com.lookalikeme.app.Variables;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by AQEEL on 5/22/2018.
 */

public class Notification_Receive extends FirebaseMessagingService {

    String  pic;
    String  title;
    String  message;
    String senderid;
    String receiverid;
    String group_id;
    SharedPreferences sharedPreferences;

    @SuppressLint("WrongThread")
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage.getData().size() > 0) {
            sharedPreferences=getSharedPreferences(Variables.pref_name,MODE_PRIVATE);
            title = remoteMessage.getData().get("title");
            message = remoteMessage.getData().get("body");
            pic=remoteMessage.getData().get("icon");
            senderid=remoteMessage.getData().get("senderid");
            receiverid=remoteMessage.getData().get("receiverid");
                // it is the notification from user to user chat
                if(!Chat_Activity.senderid_for_check_notification.equals(senderid)){
                    // if the user does not open the chat
                       // if all the notification is on
                        new sendNotification(this).execute(pic);
                    }

            }


        }



    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        sharedPreferences= getSharedPreferences(Variables.pref_name,MODE_PRIVATE);
        sharedPreferences.edit().putString(Variables.device_token,s).commit();

    }

    private class sendNotification extends AsyncTask<String, Void, Bitmap> {

        Context ctx;

        public sendNotification(Context context) {
            super();
            this.ctx = context;
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            // in notification first we will get the image of the user and then we will show the notification to user
            // in onPostExecute
            InputStream in;
            try {

                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                in = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(in);
                return myBitmap;


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {

            super.onPostExecute(result);
            try {
                // The id of the channel.
                final String CHANNEL_ID = "default";
                final String CHANNEL_NAME = "Default";

                Intent notificationIntent = new Intent(ctx, MainMenuActivity.class);
                notificationIntent.putExtra("Sender_Id",senderid);
                notificationIntent.putExtra("Receiver_Id",receiverid);
                notificationIntent.putExtra("group_id",group_id);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                PendingIntent pendingIntent = PendingIntent.getActivity(ctx, 0,
                        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(ctx.NOTIFICATION_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel defaultChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
                    notificationManager.createNotificationChannel(defaultChannel);
                }


                Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder builder = (NotificationCompat.Builder) new NotificationCompat.Builder(ctx,CHANNEL_ID);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder.setSmallIcon(R.drawable.ic_b);
                } else {
                    builder.setSmallIcon(R.drawable.ic_b);
                }
                builder.setStyle(new NotificationCompat.BigTextStyle().bigText(title))
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setLargeIcon(result).setContentTitle(title)
                        .setPriority(Notification.PRIORITY_MAX)
                        .setContentText(message).setAutoCancel(true).setSound(soundUri)
                        .setContentIntent(pendingIntent);

                Notification notification = builder.build();
                notification.defaults |= Notification.DEFAULT_VIBRATE;
                notification.flags |= Notification.FLAG_SHOW_LIGHTS;
                notificationManager.notify(1, notification);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }}




}
