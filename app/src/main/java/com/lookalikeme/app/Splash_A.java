package com.lookalikeme.app;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.lookalikeme.app.Accounts.Enable_location_F;
import com.lookalikeme.app.Accounts.Login_A;
import com.lookalikeme.app.Main_Menu.MainMenuActivity;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.kserno.geoloc.view.GeolocationActivity;

public class Splash_A extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_);
        sharedPreferences=getSharedPreferences(Variables.pref_name,MODE_PRIVATE);

        Intent intent = new Intent(this, GeolocationActivity.class);
        startActivityForResult(intent, GeolocationActivity.REQUEST_CODE);

    }



    public void GPSStatus(){
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if(!GpsStatus)
        {
            Toast.makeText(this, "On Location in High Accuracy", Toast.LENGTH_SHORT).show();
            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS),2);

        }else {

            GetCurrentlocation();
        }
    }



    private FusedLocationProviderClient mFusedLocationClient;
    private void GetCurrentlocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
           // here if user did not give the permission of location then we move user to enable location screen
            enable_location();
            return;
        }

        // else we will get the location and save it in local and move to main Screen
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            SharedPreferences.Editor editor=sharedPreferences.edit();
                            editor.putString(Variables.Lat,""+location.getLatitude());
                            editor.putString(Variables.Lon,""+location.getLongitude());
                            editor.commit();
                            startActivity(new Intent(Splash_A.this, MainMenuActivity.class));
                            overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                            finish();
                        }
                        else  {
                            SharedPreferences.Editor editor=sharedPreferences.edit();
                            editor.putString(Variables.Lat,"33.738045");
                            editor.putString(Variables.Lon,"73.084488");
                            editor.commit();
                            startActivity(new Intent(Splash_A.this, MainMenuActivity.class));
                            overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                            finish();
                        }
                    }
                });
    }


    private void enable_location() {
        Enable_location_F enable_location_f = new Enable_location_F();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left,R.anim.in_from_left,R.anim.out_to_right);
        getSupportFragmentManager().popBackStackImmediate();
        transaction.replace(R.id.Login_A, enable_location_f).addToBackStack(null).commit();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==2){
            GPSStatus();
        }
        if (requestCode == GeolocationActivity.REQUEST_CODE) {

            if(sharedPreferences.getBoolean(Variables.islogin,false)){
                // if user is already login then we get the current location of user
                GetCurrentlocation();
                GPSStatus();

            } else {
                // else we will move the user to login screen
                startActivity(new Intent(Splash_A.this, Login_A.class));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                finish();
            }

        }

    }
}
