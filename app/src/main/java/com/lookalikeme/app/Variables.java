package com.lookalikeme.app;

/**
 * Created by AQEEL on 10/23/2018.
 */

public class Variables {

    public static String pref_name="pref_name";
    public static String f_name="f_name";
    public static String l_name="l_name";
    public static String birth_day="birth_day";
    public static String gender="gender";
    public static String uid="uid";
    public static String islogin="islogin";
    public static String Lat="Lat";
    public static String Lon="Lon";
    public static String device_token="device_token";
    public static String ispuduct_puchase="ispuduct_puchase";

    public static String show_me="show_me";
    public static String max_distance="max_distance";
    public static String max_age="max_age";
    public static String min_age="min_age";
    public static String show_me_on_binder="show_me_on_tinder";



    public static String Pic_firstpart="https://graph.facebook.com/";
    public static String Pic_secondpart="/picture?width=500&width=500";

    public static String gif_firstpart="https://media.giphy.com/media/";
    public static String gif_secondpart="/100w.gif";

    public static String gif_firstpart_chat="https://media.giphy.com/media/";
    public static String gif_secondpart_chat="/200w.gif";

    public static String gif_api_key1="zOPiODd27MJ90kJ8JIrnEo9DBbLbj3je";



    // Bottom two variable Related with in App Subscription

    //First step get licencekey
    public static String licencekey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAo2kvUmMfo/OlQeLhj1sN/HKOocbsRiCwjdVIkXAPzZaH881olTQUBZdoTdbkUNjKDCHM3oe2OPfkEHbLkDrfYZQTFX/yo/SiXGVuOw6qOz8a2j6j/kljsQ8uJCa4OKlRD2bGrXasvC0zNhHCB9LKDPqrGE4ukMxn1OS12a1BR5PWyiV86Bkw/GNYgFVnvDJMm7IAd3/MSPkSrALwKlZNz2OkebnQLaQBwUYSHYBFK3ZRlDbIYG4QwLGZOKOiVo0Prxb37ZmG8QvT7bilS6gT5KIDHYbci7k7gt1TAsfBeoXhTQcbLMpb2BZEA1C/A7tjXKB+V9zBpxbdsG3tYyNZsQIDAQAB";

    //create the Product id or in app subcription id
    public static String product_ID="android.binder.subscription";





    public static int Select_image_from_gallry_code=3;


    public static String domain="https://llm.cloudmoney-llc.com/index.php?p=";

    public static String SignUp=domain+"signup";

    public static String Edit_profile=domain+"edit_profile";

    public static String getUserInfo=domain+"getUserInfo";

    public static String uploadImages=domain+"uploadImages";

    public static String deleteImages=domain+"deleteImages";

    public static String userNearByMe=domain+"userNearByMe";

    public static String show_or_hide_profile=domain+"show_or_hide_profile";


}
