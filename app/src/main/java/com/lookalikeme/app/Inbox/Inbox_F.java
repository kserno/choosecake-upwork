package com.lookalikeme.app.Inbox;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lookalikeme.app.Chat.Chat_Activity;
import com.lookalikeme.app.Functions;
import com.lookalikeme.app.Main_Menu.MainMenuActivity;
import com.lookalikeme.app.Main_Menu.RelateToFragment_OnBack.RootFragment;
import com.lookalikeme.app.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Inbox_F extends RootFragment {


    View view;
    Context context;

    RecyclerView inbox_list,match_list;

    ArrayList<Inbox_Get_Set> inbox_arraylist;

    ArrayList<Match_Get_Set>  matchs_users_list;

    DatabaseReference root_ref;

    Matches_Adapter matches_adapter;
    Inbox_Adapter inbox_adapter;

    public Inbox_F() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_inbox, container, false);
        context=getContext();

        root_ref=FirebaseDatabase.getInstance().getReference();


        inbox_list=view.findViewById(R.id.inboxlist);
        match_list=view.findViewById(R.id.match_list);

        // intialize the arraylist and and inboxlist
        inbox_arraylist=new ArrayList<>();

        inbox_list = (RecyclerView) view.findViewById(R.id.inboxlist);
        LinearLayoutManager layout = new LinearLayoutManager(context);
        inbox_list.setLayoutManager(layout);
        inbox_list.setHasFixedSize(false);
        inbox_adapter=new Inbox_Adapter(context, inbox_arraylist, new Inbox_Adapter.OnItemClickListener() {
            @Override
            public void onItemClick(Inbox_Get_Set item) {
                chatFragment(MainMenuActivity.user_id,item.getId(),item.getName(),false);


            }
        }, new Inbox_Adapter.OnLongItemClickListener() {
            @Override
            public void onLongItemClick(Inbox_Get_Set item) {

            }
        });
        inbox_list.setAdapter(inbox_adapter);





        // intialize the arraylist and and upper Match list
        match_list=view.findViewById(R.id.match_list);
        matchs_users_list=new ArrayList<>();

        match_list.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));

        matches_adapter=new Matches_Adapter(context, matchs_users_list, new Matches_Adapter.OnItemClickListener() {
            @Override
            public void onItemClick(Match_Get_Set item) {

                chatFragment(MainMenuActivity.user_id,item.getU_id(),item.getUsername(),true);

            }
        }, new Matches_Adapter.OnLongItemClickListener() {
            @Override
            public void onLongItemClick(Match_Get_Set item) {

            }
        });

        match_list.setAdapter(matches_adapter);



        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.hideSoftKeyboard(getActivity());
            }
        });

        return view;
    }


    // whenever there is focus in the third tab we will get the match list
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            get_match_data();
        }
    }


    // this mehtod will get the Match user this data is show in upper horizantal list of third tab
    ValueEventListener eventListener;
    Query query;
    public void get_match_data() {

       query= root_ref.child("Match").child(MainMenuActivity.user_id)
                .orderByChild("match").equalTo(true);
        query.keepSynced(true);
       eventListener=new ValueEventListener() {
           @Override
           public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
               matchs_users_list.clear();
               for (DataSnapshot ds:dataSnapshot.getChildren()) {
                   if(ds.child("status").getValue()!=null){

                       if(ds.child("status").getValue().toString().equals("0")){
                   Match_Get_Set model = new Match_Get_Set();
                   model.setU_id(ds.getKey());
                   model.setUsername(ds.child("name").getValue().toString());
                   matchs_users_list.add(model);

                       }
               }

               }
               matches_adapter.notifyDataSetChanged();

               if(matchs_users_list.isEmpty()){
                   view.findViewById(R.id.no_match_txt).setVisibility(View.VISIBLE);
               }else {
                   view.findViewById(R.id.no_match_txt).setVisibility(View.GONE);
               }
           }

           @Override
           public void onCancelled(@NonNull DatabaseError databaseError) {

           }
       };

     query.addListenerForSingleValueEvent(eventListener);


    }



    // on start we will get the Inbox Message of user  which is show in bottom list of third tab
    ValueEventListener eventListener2;
    @Override
    public void onStart() {
        super.onStart();
        eventListener2=new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                inbox_arraylist.clear();
                for (DataSnapshot ds:dataSnapshot.getChildren()) {
                    Inbox_Get_Set model = new Inbox_Get_Set();
                    model.setId(ds.getKey());
                    model.setName(ds.child("name").getValue().toString());
                    model.setMessage(ds.child("msg").getValue().toString());
                    model.setTimestamp(ds.child("date").getValue().toString());
                    model.setStatus(ds.child("status").getValue().toString());
                    inbox_arraylist.add(model);
                    inbox_adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        root_ref.child("Inbox").child(MainMenuActivity.user_id).addValueEventListener(eventListener2);


    }

    // on stop we will remove the listener
    @Override
    public void onStop() {
        super.onStop();
        root_ref.child("Inbox").child(MainMenuActivity.user_id).removeEventListener(eventListener2);
    }

    //open the chat fragment and on item click and pass your id and the other person id in which
    //you want to chat with them and this parameter is that is we move from match list or inbox list
    public void chatFragment(String senderid,String receiverid,String name,boolean is_match_exits){
        Chat_Activity chat_activity = new Chat_Activity();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
       // transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
        Bundle args = new Bundle();
        args.putString("Sender_Id",senderid);
        args.putString("Receiver_Id",receiverid);
        args.putString("name",name);
        args.putBoolean("is_match_exits",is_match_exits);
        chat_activity.setArguments(args);
        transaction.addToBackStack(null);
        transaction.replace(R.id.MainMenuFragment, chat_activity).commit();
    }





}
