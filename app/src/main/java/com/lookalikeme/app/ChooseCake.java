package com.lookalikeme.app;

import android.app.Application;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by AQEEL on 10/19/2018.
 */

public class ChooseCake extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
        // line used for store the firebase data offline
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

    }
}
