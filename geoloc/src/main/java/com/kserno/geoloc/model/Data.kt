package com.kserno.geoloc.model

import com.squareup.moshi.JsonClass

/**
 *  Created by filipsollar on 21/11/2018
 */
@JsonClass(generateAdapter = true)
data class Data(
        val geo: String,
        val url: String
)