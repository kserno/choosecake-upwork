package com.kserno.geoloc.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 *  Created by filipsollar on 21/11/2018
 */
@JsonClass(generateAdapter = true)
data class IpGeolocation(
        @Json(name = "as") val name: String,
        val city: String,
        val country: String,
        val countryCode: String,
        val isp: String,
        val lat: Double,
        val lon: Double,
        val org: String,
        val query: String,
        val region: String,
        val regionName: String,
        val status: String,
        val timezone: String,
        val zip: String
)