package com.kserno.geoloc

import android.content.Context

/**
 *  Created by filipsollar on 2019-02-04
 */
class CookiesDatabase(context: Context) {

    companion object {
        const val PREFS_NAME = "PREFS_LOOKALIKEME"

        const val PREF_COOKIES = "PREF_COOKIES"
        const val DELIMITER = ";"
    }

    private val sharedPrefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    private val cookies = HashMap<String, String>()

    init {
        val cookiesStr = sharedPrefs.getString(PREF_COOKIES, "")
        if (!cookiesStr.isEmpty()) {
            val cookiesList = cookiesStr.split(DELIMITER)
            for (cookie in cookiesList) {
                cookies[cookie] = sharedPrefs.getString(cookie, "")
            }
        }
    }

    fun addCookie(siteUrl: String, cookie: String) {
        cookies[siteUrl] = cookie

        val keys = cookies.keys.joinToString(DELIMITER)

        sharedPrefs.edit()
                .putString(PREF_COOKIES, keys)
                .apply()

        sharedPrefs.edit()
                .putString(siteUrl, cookie)
                .apply()
    }

    fun getCookies(siteUrl: String): String? {
        return cookies[siteUrl]
    }

}