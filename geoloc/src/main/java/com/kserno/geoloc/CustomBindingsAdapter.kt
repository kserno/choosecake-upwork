package com.kserno.geoloc

import android.databinding.BindingAdapter
import android.view.View

/**
 *  Created by filipsollar on 2019-02-04
 */
object CustomBindingsAdapter {

    @JvmStatic
    @BindingAdapter("animateInvisibilityIf")
    fun View.animateInvisibilityIf(condition: Boolean) {
        val alpha = if (condition) {
            0f
        } else {
            1f
        }

        this.animate()
                .alpha(alpha)
                .setDuration(1000L)
                .start()
    }

}