package com.kserno.geoloc.view

import android.app.Activity
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.kserno.geoloc.R
import com.kserno.geoloc.network.GeolocationService
import kotlinx.android.synthetic.main.activity_geolocation.*
import android.os.Build
import android.annotation.TargetApi
import android.net.http.SslError
import android.os.PersistableBundle
import android.util.Log
import android.webkit.CookieManager
import android.webkit.CookieSyncManager
import android.webkit.SslErrorHandler
import android.webkit.WebChromeClient
import android.webkit.WebResourceError
import android.webkit.WebResourceResponse
import android.webkit.WebSettings
import android.widget.Toast
import com.kserno.geoloc.CookiesDatabase
import com.kserno.geoloc.databinding.ActivityGeolocationBinding
import java.net.MalformedURLException
import java.net.URL


/**
 *  Created by filipsollar on 21/11/2018
 */
class GeolocationActivity: AppCompatActivity(), GeolocationView {

    private lateinit var viewModel: GeolocationViewModel
    private lateinit var binding: ActivityGeolocationBinding
    private lateinit var cookiesDatabase: CookiesDatabase

    companion object {
        const val REQUEST_CODE = 0

        const val RESULT_SKIPPED = 1
        const val RESULT_ERROR = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_geolocation)
        viewModel = GeolocationViewModel(GeolocationService(), this)
        cookiesDatabase = CookiesDatabase(this)

        binding.viewModel = viewModel
    }

    override fun loadWebViewUrl(url: String) {
        webView.settings.javaScriptEnabled = true
        webView.settings.domStorageEnabled = true
        val test = webView.settings.userAgentString

        webView.settings.userAgentString = test.replace("; wv", "")
        webView.clearSslPreferences()

        CookieManager.getInstance().setCookie(url, cookiesDatabase.getCookies(getBaseUrl(url)))

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            webView.settings.safeBrowsingEnabled = false

        }
        webView.settings.javaScriptCanOpenWindowsAutomatically = true
        if (Build.VERSION.SDK_INT >= 21) {
            webView.settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
        }
        webView.webChromeClient = object: WebChromeClient() {


        }
        //CookiesDatabase.getInstance().setCookie()
        webView.webViewClient = object: WebViewClient() {

            override fun onReceivedSslError(view: WebView?, handler: SslErrorHandler?, error: SslError?) {
                super.onReceivedSslError(view, handler, error)
                handler?.proceed()
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                if (viewModel.isLoading) {
                    viewModel.isLoading = false
                }
                val cookies = CookieManager.getInstance().getCookie(url)

                if (url != null && cookies != null) {
                    cookiesDatabase.addCookie(getBaseUrl(url), cookies)
                }
            }

            @TargetApi(Build.VERSION_CODES.N)
            override fun shouldOverrideUrlLoading(webView: WebView, request: WebResourceRequest): Boolean {
                val uri = request.url
                return shouldOverrideUrlLoading(webView, uri.toString())
            }

            override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                Log.i("TAG", error.toString())
                super.onReceivedError(view, request, error)
            }


            override fun onReceivedHttpError(view: WebView?, request: WebResourceRequest?, errorResponse: WebResourceResponse?) {
                super.onReceivedHttpError(view, request, errorResponse)
            }

            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                return false
            }
        }
        webView.loadUrl(url)
    }

    override fun onResume() {
        super.onResume()
        if (viewModel.timerDisposable != null) {
            //viewModel.startTimer() // timer is disposed
        }
    }

    override fun onStop() {
        super.onStop()
        viewModel.onStop()
    }

    override fun onBackPressed() {
        //super.onBackPressed() don't allow back press
    }

    override fun closeError() {
        setResult(RESULT_ERROR)
        finish()
    }

    override fun close() {
        setResult(RESULT_SKIPPED)
        finish()
    }

    private fun getBaseUrl(stringUrl: String) : String {
        try {
            val url = URL(stringUrl);
            return url.protocol + "://" + url.host
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        }
        return stringUrl
    }
}