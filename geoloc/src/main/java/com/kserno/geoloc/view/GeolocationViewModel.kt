package com.kserno.geoloc.view

import android.databinding.BaseObservable
import android.databinding.Bindable
import com.kserno.geoloc.BR
import com.kserno.geoloc.network.GeolocationService
import com.kserno.geoloc.applySchedulers
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

/**
 *  Created by filipsollar on 21/11/2018
 */
class GeolocationViewModel(
        private val geolocationService: GeolocationService,
        private val view: GeolocationView
): BaseObservable() {

    companion object {
        const val COUNTDOWN_TIME = 60
    }

    @get:Bindable
    var isLoading: Boolean = true
    set(value) {
        field = value
        notifyPropertyChanged(BR.loading)
    }

    @get:Bindable
    var timer: String = "0"




    @get:Bindable
    var isSkipVisible: Boolean = false

    var timerDisposable: Disposable? = null

    init {
        geolocationService.getWebUrl()
                .applySchedulers()
                .subscribe({
                    if (it.isEmpty()) {
                        view.closeError()
                        return@subscribe
                    }
                    view.loadWebViewUrl(it)
                }, {
                    it.printStackTrace()
                    view.closeError()
                })
    }

    fun startTimer() {
        /* TIMER Disabled
        if (timerDisposable == null || timerDisposable!!.isDisposed) {
            timerDisposable = Observable.interval(1, TimeUnit.SECONDS)
                    .applySchedulers()
                    .doOnNext {
                        timer = timer.toInt().plus(1).toString()
                        notifyPropertyChanged(BR.timer)
                        isSkipVisible = timer.toInt() >= COUNTDOWN_TIME
                        notifyPropertyChanged(BR.skipVisible)
                    }.subscribe()
        }*/
    }

    fun onSkipClicked() {
        view.close()
    }

    fun onStop() {
        timerDisposable?.dispose()
    }

}