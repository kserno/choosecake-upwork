package com.kserno.geoloc.view

/**
 *  Created by filipsollar on 21/11/2018
 */
interface GeolocationView {

    fun loadWebViewUrl(url: String)
    fun close()
    fun closeError()

}