package com.kserno.geoloc.network.api

import com.kserno.geoloc.model.Data
import io.reactivex.Single
import retrofit2.http.GET

/**
 *  Created by filipsollar on 21/11/2018
 */
interface GeolocationApi {

    @GET("/geo.json")
    fun loadGeolocationUrls() : Single<List<Data>>

}