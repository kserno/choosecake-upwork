package com.kserno.geoloc.network.api

import com.kserno.geoloc.model.IpGeolocation
import io.reactivex.Single
import retrofit2.http.GET

/**
 *  Created by filipsollar on 21/11/2018
 */
interface IpApi {

    @GET("/json/")
    fun geoLocateIp() : Single<IpGeolocation>

}