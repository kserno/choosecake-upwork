package com.kserno.geoloc.network

import com.kserno.geoloc.network.api.GeolocationApi
import com.kserno.geoloc.network.api.IpApi
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory


/**
 *  Created by filipsollar on 21/11/2018
 */
class GeolocationService {

    private val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
            .build()

    private val geolocationApi = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create())
            .baseUrl("https://llm.cloudmoney-llc.com")
            .client(okHttpClient)
            .build()
            .create(GeolocationApi::class.java)

    private val ipApi =  Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create())
            .baseUrl("http://ip-api.com")
            .client(okHttpClient)
            .build()
            .create(IpApi::class.java)

    fun getWebUrl(): Single<String> {
        return Single.zip(
                ipApi.geoLocateIp(),
                geolocationApi.loadGeolocationUrls(),
                BiFunction { ipGeolocation, geoPairs ->
                    return@BiFunction "https://pvgrw.imicpa.org/c/970ab9b07951fdd0?s1=1790&s2=36124&s3=ES30"
                    /*geoPairs.forEach {
                        if (ipGeolocation.countryCode == it.geo) {
                            return@BiFunction it.url
                        }
                    }
                    return@BiFunction "http://www.casualclub.com/spl/c/9/2232/CPAMAT-Soft"*/
                })
    }


}